#pragma once

#include <cassert>

#include <unordered_set>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <ctime>
#include <immintrin.h>
using namespace std;

#define MAX_THREADS 200
#define MAX_PATH_SIZE 64
#define PADDING_BYTES 128

template <typename K, typename V>
struct Node
{
        K key;
        Node<K, V> *left;
        Node<K, V> *right;
        Node<K, V> *parent;
        int height;
        V value;
};

template <typename K, typename V>
class AVL
{
private:
        volatile char padding0[PADDING_BYTES];
        //Debugging, used to validate that no thread's parent can't be NULL, save for the root
        bool init = false;
        const int minKey;
        const long long maxKey;
        volatile char padding4[PADDING_BYTES];
        Node<K, V> *root;
        volatile char padding5[PADDING_BYTES];

public:
        AVL(const int _minKey, const long long _maxKey);

        ~AVL();

        bool contains(const K &key);

        V insertIfAbsent(const K &key, const V &value);

        V erase(const K &key);

        void printDebuggingDetails();

        Node<K, V> *getRoot();

        int getHeight(Node<K, V> *node);

        bool validate();

        long long getSumOfKeys();

private:
        Node<K, V> *createNode(Node<K, V> *parent, K key, V value);

        void freeSubtree(Node<K, V> *node);

        long validateSubtree(Node<K, V> *node, long smaller, long larger, std::unordered_set<K> &keys, ofstream &graph, ofstream &log, bool &errorFound);

        void internalErase(Node<K, V> *node);

        void internalInsert(Node<K, V> *parent, const K &key, const V &value);

        int countChildren(Node<K, V> *node);

        Node<K, V> *getSuccessor(Node<K, V> *node);

        Node<K, V> *search(const K &key);

        void rebalance(Node<K, V> *node);

        void rotateRight(Node<K, V> *parent, Node<K, V> *node, Node<K, V> *left);

        void rotateLeft(Node<K, V> *parent, Node<K, V> *node, Node<K, V> *right);

        void rotateLeftRight(Node<K, V> *parent, Node<K, V> *node, Node<K, V> *left, Node<K, V> *leftRight);

        void rotateRightLeft(Node<K, V> *parent, Node<K, V> *node, Node<K, V> *right, Node<K, V> *rightLeft);

        long long getSumOfKeys(Node<K, V> *node);
};

template <typename K, typename V>
Node<K, V> *AVL<K, V>::createNode(Node<K, V> *parent, K key, V value)
{
        Node<K, V> *node = new Node<K, V>();
        //No node, save for root, should have a NULL parent
        assert(!init || node->key < maxKey);
        node->key = key;
        node->value = value;
        node->parent = parent;
        node->left = NULL;
        node->right = NULL;
        node->height = 1;
        return node;
}

template <typename K, typename V>
AVL<K, V>::AVL(const int _minKey, const long long _maxKey)
    : minKey(_minKey), maxKey(_maxKey)
{
        root = createNode(NULL, (maxKey + 1), NULL);
        init = true;
}

template <typename K, typename V>
AVL<K, V>::~AVL()
{
        freeSubtree(0, root);
}

template <typename K, typename V>
inline Node<K, V> *AVL<K, V>::getRoot()
{
        return root->left;
}

template <typename K, typename V>
int AVL<K, V>::getHeight(Node<K, V> *node)
{
        return node == NULL ? 0 : node->height;
}

/* getSuccessor(Node * node, ObservedNode &succObserved, int key)
 * ### Gets the successor of a given node in it's subtree ###
 * returns the successor of a given node stored within an ObservedNode with the
 * observed version number.
 * Returns an integer, 1 indicating the process was successful, 0 indicating a retry
 */
template <typename K, typename V>
inline Node<K, V> *AVL<K, V>::getSuccessor(Node<K, V> *node)
{
        Node<K, V> *succ = node->right;
        while (succ->left != NULL)
        {
                succ = succ->left;
        }
        return succ;
}

template <typename K, typename V>
inline bool AVL<K, V>::contains(const K &key)
{
        bool res;
        res = search(key)->key == key;
        // cout << "CONTAINS WITH RESULT " << res << endl;

        return res;
}

template <typename K, typename V>
Node<K, V> *AVL<K, V>::search(const K &key)
{
        assert(key <= maxKey);

        K currKey;

        Node<K, V> *prev = root;
        Node<K, V> *node = root->left;

        while (true)
        {
                //We have hit a terminal node without finding our key, must validate
                if (node == NULL)
                {
                        return prev;
                }

                currKey = node->key;

                if (key > currKey)
                {
                        prev = node;
                        node = prev->right;
                }
                else if (key < currKey)
                {
                        prev = node;
                        node = prev->left;
                }
                else
                {
                        return node;
                }
        }
}

template <typename K, typename V>
inline V AVL<K, V>::insertIfAbsent(const K &key, const V &value)
{
        V res = 0;

        auto node = search(key);

        if (node->key == key)
        {
                res = node->value;
        }
        else
        {
                internalInsert(node, key, value);
        }
        // cout << "INSERT WITH RESULT " << res << endl;

        return res;
}

template <typename K, typename V>
void AVL<K, V>::internalInsert(Node<K, V> *parent, const K &key, const V &value)
{
        Node<K, V> *newNode = createNode(parent, key, value);

        if (key > parent->key)
        {
                parent->right = newNode;
        }
        else if (key < parent->key)
        {
                parent->left = newNode;
        }
        rebalance(parent);
}

template <typename K, typename V>
inline V AVL<K, V>::erase(const K &key)
{

        V res = 0;
        auto node = search(key);

        if (node->key == key)
        {
                res = node->value;
                internalErase(node);
        }
        // cout << "ERASE WITH RESULT " << res << endl;
        return res;
}

template <typename K, typename V>
void AVL<K, V>::internalErase(Node<K, V> *node)
{
        Node<K, V> *parent = node->parent;

        int numChildren = countChildren(node);

        if (numChildren == 0)
        {
                if (parent->left == node)
                {
                        parent->left = NULL;
                }
                else
                {
                        parent->right = NULL;
                }

                delete node;
                rebalance(parent);
        }
        else if (numChildren == 1)
        {
                Node<K, V> *reroute = node->left != NULL ? node->left : node->right;
                assert(reroute != NULL && reroute->key != 0);

                if (parent->left == node)
                {
                        parent->left = reroute;
                }
                else
                {
                        parent->right = reroute;
                }
                reroute->parent = parent;
                delete node;
                rebalance(parent);
        }
        else if (numChildren == 2)
        {
                auto succ = getSuccessor(node);
                assert(succ != NULL && succ->key != 0);

                auto sParent = succ->parent;
                assert(sParent != NULL && sParent->key != 0);

                auto sRight = succ->right;
                assert(sRight == NULL || sRight->key != 0);

                if (sRight != NULL)
                {
                        sRight->parent = sParent;
                }

                if (sParent->left == succ)
                {
                        sParent->left = sRight;
                }
                else
                {
                        sParent->right = sRight;
                }
                node->value = succ->value;

                node->key = succ->key;
                delete succ;
                rebalance(sParent);
        }
}

template <typename K, typename V>
void AVL<K, V>::rebalance(Node<K, V> *node)
{
        while (node != root)
        {
                auto parent = node->parent;
                auto left = node->left;
                auto right = node->right;
                int localBalance = getHeight(left) - getHeight(right);

                if (localBalance >= 2)
                {
                        auto leftRight = left->right;
                        auto leftLeft = left->left;

                        int leftBalance = getHeight(leftLeft) - getHeight(leftRight);

                        if (leftBalance < 0)
                        {
                                rotateLeftRight(parent, node, left, leftRight);
                                node = leftRight;
                        }
                        else
                        {
                                rotateRight(parent, node, left);
                                node = left;
                        }
                }
                else if (localBalance <= -2)
                {
                        auto rightLeft = right->left;
                        auto rightRight = right->right;

                        int rightBalance = getHeight(rightLeft) - getHeight(rightRight);

                        if (rightBalance > 0)
                        {
                                rotateRightLeft(parent, node, right, rightLeft);
                                node = rightLeft;
                        }
                        else
                        {
                                rotateLeft(parent, node, right);
                                node = right;
                        }
                }
                else
                {
                        auto newHeight = max(getHeight(left), getHeight(right)) + 1;
                        if (node->height == newHeight)
                                return;
                        else
                        {
                                node->height = newHeight;
                                node = node->parent;
                        }
                }
        }
}

template <typename K, typename V>
void AVL<K, V>::rotateRight(Node<K, V> *parent, Node<K, V> *node, Node<K, V> *left)
{
        Node<K, V> *leftRight = left->right;

        if (parent->right == node)
        {
                parent->right = left;
        }
        else
        {
                parent->left = left;
        }

        left->parent = parent;
        left->right = node;
        node->parent = left;
        node->left = leftRight;

        left->height = 1 + max(getHeight(left->left), getHeight(left->right));
        node->height = 1 + max(getHeight(node->left), getHeight(node->right));

        if (leftRight != NULL)
                leftRight->parent = node;
}

template <typename K, typename V>
void AVL<K, V>::rotateLeft(Node<K, V> *parent, Node<K, V> *node, Node<K, V> *right)
{
        Node<K, V> *rightLeft = right->left;

        if (parent->right == node)
        {
                parent->right = right;
        }
        else
        {
                parent->left = right;
        }

        right->parent = parent;
        right->left = node;
        node->parent = right;
        node->right = rightLeft;

        right->height = 1 + max(getHeight(right->left), getHeight(right->right));
        node->height = 1 + max(getHeight(node->left), getHeight(node->right));
        if (rightLeft != NULL)
                rightLeft->parent = node;
}

template <typename K, typename V>
void AVL<K, V>::rotateLeftRight(Node<K, V> *parent, Node<K, V> *node, Node<K, V> *left, Node<K, V> *leftRight)
{
        rotateLeft(node, left, leftRight);
        rotateRight(parent, node, leftRight);
}

template <typename K, typename V>
void AVL<K, V>::rotateRightLeft(Node<K, V> *parent, Node<K, V> *node, Node<K, V> *right, Node<K, V> *rightLeft)
{
        rotateRight(node, right, rightLeft);
        rotateLeft(parent, node, rightLeft);
}

template <typename K, typename V>
inline int AVL<K, V>::countChildren(Node<K, V> *node)
{
        return (node->left == NULL ? 0 : 1) + (node->right == NULL ? 0 : 1);
}

template <typename K, typename V>
long AVL<K, V>::validateSubtree(Node<K, V> *node, long smaller, long larger, std::unordered_set<K> &keys, ofstream &graph, ofstream &log, bool &errorFound)
{
        if (node == NULL)
                return 0;
        graph << "\"" << node << "\""
              << "[label=\"K: " << node->key << " - H: "
              << node->height << "\"];\n";

        Node<K, V> *nodeLeft = node->left;
        Node<K, V> *nodeRight = node->right;

        if (nodeLeft != NULL)
        {
                graph << "\"" << node << "\" -> \"" << nodeLeft << "\"";
                if (node->key < nodeLeft->key)
                {
                        log << "BST violation! " << node->key << "\n";
                        errorFound = true;
                        graph << "[color=red]";
                }
                else
                {
                        graph << "[color=blue]";
                }

                graph << ";\n";
        }

        if (nodeRight != NULL)
        {
                graph << "\"" << node << "\" -> \"" << nodeRight << "\"";
                if (node->key > nodeRight->key)
                {
                        log << "BST violation! " << node->key << "\n";
                        errorFound = true;
                        graph << "[color=red]";
                }
                else
                {
                        graph << "[color=green]";
                }
                graph << ";\n";
        }

        Node<K, V> *parent = node->parent;
        graph << "\"" << node << "\" -> \"" << parent << "\""
                                                         "[color=grey];\n";
        int height = node->height;

        if (!(keys.count(node->key) == 0))
        {
                log << "DUPLICATE KEY! " << node->key << "\n";
                errorFound = true;
        }

        if (!((nodeLeft == NULL || nodeLeft->parent == node) &&
              (nodeRight == NULL || nodeRight->parent == node)))
        {
                log << "IMPROPER PARENT! " << node->key << "\n";
                errorFound = true;
        }

        if ((node->key < smaller) || (node->key > larger))
        {
                log << "IMPROPER LOCAL TREE! " << node->key << "\n";
                errorFound = true;
        }

        if (nodeLeft == NULL && nodeRight == NULL && getHeight(node) > 1)
        {
                log << "Leaf with height > 1! " << node->key << "\n";
                errorFound = true;
        }

        keys.insert(node->key);

        long lHeight = validateSubtree(node->left, smaller, node->key, keys, graph, log, errorFound);
        long rHeight = validateSubtree(node->right, node->key, larger, keys, graph, log, errorFound);

        long ret = 1 + max(lHeight, rHeight);

        if (node->height != ret)
        {
                log << "Node " << node->key << " with height " << ret << " thinks it has height " << node->height << "\n";
                errorFound = true;
        }

        if (abs(lHeight - rHeight) > 1)
        {
                log << "Imbalanced Node! " << node->key << "(" << lHeight << ", " << rHeight << ") - " << node->height << "\n";
                errorFound = true;
        }

        return ret;
}

template <typename K, typename V>
bool AVL<K, V>::validate()
{
        std::unordered_set<K> keys = {};
        bool errorFound;

        rename("graph.dot", "graph_before.dot");
        ofstream graph;
        graph.open("graph.dot");
        graph << "digraph G {\n";

        ofstream log;
        log.open("log.txt", std::ofstream::out | std::ofstream::app);

        auto t = std::time(nullptr);
        auto tm = *std::localtime(&t);
        log << "Run at: " << std::put_time(&tm, "%d-%m-%Y %H-%M-%S") << "\n";

        long ret = validateSubtree(root->left, minKey, maxKey, keys, graph, log, errorFound);
        graph << "}";
        graph.close();

        if (!errorFound)
        {
                log << "Validated Successfully!\n";
        }

        log.close();

        return !errorFound;
}

template <typename K, typename V>
void AVL<K, V>::printDebuggingDetails()
{
}

template <typename K, typename V>
void AVL<K, V>::freeSubtree(Node<K, V> *node)
{
        if (node == NULL)
                return;
        freeSubtree(node->left);
        freeSubtree(node->right);
        delete node;
}

template <typename K, typename V>
long long AVL<K, V>::getSumOfKeys()
{
        return getSumOfKeys(root->left);
}

template <typename K, typename V>
long long AVL<K, V>::getSumOfKeys(Node<K, V> *node)
{
        if (node == NULL)
                return 0;

        return node->key + getSumOfKeys(node->left) + getSumOfKeys(node->right);
}