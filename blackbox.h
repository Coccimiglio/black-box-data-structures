#include "box_adapter.h"
#include "rwlock.h"
#include "locks_impl.h"

#define LOG_SIZE 1000000 //original work said 1M "worked well" -- test to see if true - NEEDS TO BE AT LEAST MAX_BATCH * (NUMA_NODES + 1)
#define PADDING_BYTES 128

#ifndef MAX_NUMA_NODES
#define MAX_NUMA_NODES 4
#endif

#ifndef MAX_THREADS_PER_NODE
#define MAX_THREADS_PER_NODE  100
#endif


#define MAX_BATCH MAX_THREADS_PER_NODE

#define BOOL_CAS __sync_bool_compare_and_swap

#define UNLIKELY __glibc_unlikely
#define LIKELY __glibc_likely

struct GlobalEntry //layout?
{
        volatile Operation op = none;
        void *volatile arg1 = (void *)0;
        void *volatile arg2 = (void *)0;
        volatile bool emptyBit = false;
};

struct LocalEntry //layout?
{
        volatile Operation op = none;
        void *volatile arg1 = (void *)0;
        void *volatile arg2 = (void *)0;
        void *volatile res = (void *)1; //must start looking like it completed this op
        volatile char padding[PADDING_BYTES];
};

template <typename K, typename V>
struct NodeReplica
{
        volatile char padding0[PADDING_BYTES];
        box_adapter<K, V> *ds;
        volatile char padding1[PADDING_BYTES];
        LocalEntry slots[MAX_THREADS_PER_NODE];
        volatile int batch[MAX_THREADS_PER_NODE];
        volatile char padding2[PADDING_BYTES];
        volatile long long tail = 0;
        volatile char padding3[PADDING_BYTES];
        TryLock combinerLock;
        volatile char padding4[PADDING_BYTES];
        RWLock rwLock;
        volatile char padding5[PADDING_BYTES];
        volatile bool empty = false;
        volatile char padding6[PADDING_BYTES];
        volatile int activeThreads = 0;
        volatile char padding7[PADDING_BYTES];
};

template <typename K, typename V>
class BlackBox
{
public:
private: //TODO: remove redundant padding - you over-did it
        volatile char padding0[PADDING_BYTES];
        const int numThreads;
        int numNodes;
        int numThreadsPerNode;
        int maxBatch;
        const int minKey;
        const long long maxKey;
        volatile char padding01[PADDING_BYTES];
        GlobalEntry log[LOG_SIZE];
        volatile char padding09[PADDING_BYTES];
        volatile long long logTail = 0;
        volatile char padding10[PADDING_BYTES];
        volatile long long logMin = LOG_SIZE - 1;
        volatile char padding11[PADDING_BYTES];
        volatile long long completedTail = 0;
        volatile char padding14[PADDING_BYTES];
        NodeReplica<K, V> replicas[MAX_NUMA_NODES];
        volatile char padding15[PADDING_BYTES];

public:
        BlackBox(const int numThreads, int _numHardwareThreadsPerSocket, const int minKey, const long long maxKey);

        ~BlackBox();

        void *executeConcurrent(const int tid, Operation op, void *arg1);

        void *executeConcurrent(const int tid, Operation op, void *arg1, void *arg2);

        bool validate();

        void initThread(const int tid);

        void deinitThread(const int tid);

private:
        long long reserveLogEntries(const int tid, int numSlots);

        long long appendToLog(const int tid, int numSlots);

        void *combine(const int tid, Operation op, void *arg1, void *arg2);

        void *readOnly(const int tid, Operation op, void *arg1, void *arg2);

        void updateFromLog(const int tid, long long endIndex);
};

template <typename K, typename V>
BlackBox<K, V>::BlackBox(const int _numThreads, int _numHardwareThreadsPerSocket, const int _minKey, const long long _maxKey) : numThreads(_numThreads), minKey(_minKey), maxKey(_maxKey)
{
        numNodes = numThreads / _numHardwareThreadsPerSocket + 1;
        cout << "Num nodes: " << numNodes << endl;
        for (int i = 0; i < numNodes; i++)
        {
                replicas[i].ds = new box_adapter<K, V>(minKey, maxKey, 0);
        }
}

template <typename K, typename V>
BlackBox<K, V>::~BlackBox()
{
        cout << "Implement destructor you hoser" << endl;
}

template <typename K, typename V>
void *BlackBox<K, V>::executeConcurrent(const int tid, Operation op, void *arg1)
{
        return executeConcurrent(tid, op, arg1, (void *)0);
}

template <typename K, typename V>
void *BlackBox<K, V>::executeConcurrent(const int tid, Operation op, void *arg1, void *arg2)
{
        if (replicas[tid / MAX_THREADS_PER_NODE].ds->isReadOnly(op))
                return readOnly(tid, op, arg1, arg2);
        return combine(tid, op, arg1, arg2);
}

template <typename K, typename V>
void BlackBox<K, V>::initThread(const int tid)
{
        __sync_fetch_and_add(&replicas[tid / MAX_THREADS_PER_NODE].activeThreads, 1);
}

template <typename K, typename V>
void BlackBox<K, V>::deinitThread(const int tid)
{

        if (__sync_fetch_and_add(&replicas[tid / MAX_THREADS_PER_NODE].activeThreads, -1) == 1)
        {
                updateFromLog(tid, completedTail);
        }
}

template <typename K, typename V>
long long BlackBox<K, V>::appendToLog(const int tid, int numSlots)
{
        int batchIndex = 0;
        long long start = reserveLogEntries(tid, numSlots);
        auto replica = &replicas[tid / MAX_THREADS_PER_NODE];
        for (int i = start; i < start + numSlots; i++)
        {
                int idx = i % LOG_SIZE;
                auto curr = &replica->slots[replica->batch[batchIndex++]];
                log[idx].arg1 = curr->arg1;
                log[idx].arg2 = curr->arg2;
                log[idx].op = curr->op;
                __sync_synchronize();
                log[idx].emptyBit = !log[idx].emptyBit; //I was empty before now, just flip me -- deserves a better explanation
        }
       //printf("%d filled slots %lld to %lld\n", tid, start, start + numSlots - 1);

        return start;
}

template <typename K, typename V>
long long BlackBox<K, V>::reserveLogEntries(const int tid, int numSlots)
{
        assert(numSlots >= 1);
        long long observedTail;
        long long minObserved;
        auto replica = &replicas[tid / MAX_THREADS_PER_NODE];

        while (true)
        {
                observedTail = logTail;
                if (BOOL_CAS(&logTail, observedTail, observedTail + numSlots))
                {
                        minObserved = logMin;
                       //printf("%d reserved slots %lld to %lld\n", tid, observedTail, observedTail + numSlots - 1);
                        while ((observedTail + numSlots) > minObserved - maxBatch)
                        {
                                long long newMin;
                                newMin = __LONG_LONG_MAX__;
                                for (int i = 0; i < numNodes; i++)
                                {
                                        int curr = replicas[i].tail;
                                        while (curr + LOG_SIZE - minObserved < maxBatch * (numNodes + 1))
                                        { //Wait until it's safe to write
                                                if (i == tid / MAX_THREADS_PER_NODE && replica->tail < completedTail)
                                                {
                                                        replica->rwLock.writeLock();
                                                        updateFromLog(tid, completedTail);
                                                        replica->rwLock.writeUnlock();
                                                }
                                                curr = replicas[i].tail;
                                        }
                                        if (curr < newMin)
                                        {
                                                newMin = curr;
                                        }
                                }

                                if (BOOL_CAS(&logMin, minObserved, newMin + LOG_SIZE))
                                {
                                     // printf("%d updates LOGMIN %lld to %lld\n", tid, minObserved, newMin + LOG_SIZE);

                                        return observedTail;
                                }
                                else
                                {
                                        minObserved = logMin;
                                }
                        }
                        return observedTail;
                }
        }
}

template <typename K, typename V>
void *BlackBox<K, V>::combine(const int tid, Operation op, void *arg1, void *arg2)
{
        assert(op != Operation::none);
        auto replica = &replicas[tid / MAX_THREADS_PER_NODE];
        auto slot = &replica->slots[tid % MAX_THREADS_PER_NODE];
        slot->op = op;
        slot->arg1 = arg1;
        slot->arg2 = arg2;
        __sync_synchronize();
        slot->res = (void *)-1; //-1 indicates that we are ready to perform this update

        while (true)
        {
                if (replica->combinerLock.tryAcquire())
                { //combiner
                        auto threadsOnNode = tid / MAX_THREADS_PER_NODE == numNodes ? numThreads % MAX_THREADS_PER_NODE : MAX_THREADS_PER_NODE;
                        //Final NUMA node might not have all the threads filled, avoid checking
                        int count = 0;
                        for (int i = 0; i < threadsOnNode; i++)
                        {
                                if (replica->slots[i].res == ((void *)-1))
                                {
                                        replica->batch[count++] = i;
                                }
                        }
                        if(count == 0) {
                                assert(slot->res != (void *)-1);
                                replica->combinerLock.release();
                                return slot->res;
                        }
                        auto startEntry = appendToLog(tid, count);
                        replica->rwLock.writeLock();
                        updateFromLog(tid, startEntry);
                        auto endEntry = startEntry + count;

                        while (true)
                        {
                                auto temp = completedTail;
                                if (endEntry < temp || BOOL_CAS(&completedTail, temp, endEntry))
                                        break;
                        }

                        for (int i = 0; i < count; i++)
                        {
                                auto curr = &replica->slots[replica->batch[i]];
                                curr->res = replica->ds->carryOutOperation(curr->op, curr->arg1, curr->arg2);
                        }

                        if (UNLIKELY(replica->tail / LOG_SIZE  != endEntry / LOG_SIZE))
                        {
                                replica->empty = !replica->empty;
                        }
                        replica->tail = endEntry;
                    

                        replica->rwLock.writeUnlock();
                        replica->combinerLock.release();
                        return slot->res;
                }
                else
                {
                        while (slot->res == (void *)-1 && replica->combinerLock.isHeld())
                        {
                                //wait
                        }
                        if (slot->res != (void *)-1)
                        {
                                return slot->res;
                        }
                }
        }
}

template <typename K, typename V>
void *BlackBox<K, V>::readOnly(const int tid, Operation op, void *arg1, void *arg2)
{
        auto readTail = completedTail; //lin here
        __sync_synchronize();
        auto replica = &replicas[tid / MAX_THREADS_PER_NODE];

        while (replica->tail < readTail)
        {
                if (replica->combinerLock.isHeld())
                {
                        while (replica->combinerLock.isHeld())
                        {
                                /*Wait for the combiner to finish*/
                        }
                }
                else
                {
                        //Update the replica ourselves
                        replica->rwLock.writeLock();
                        updateFromLog(tid, readTail);
                        replica->rwLock.writeUnlock();
                }
        }

        //do the actual read
        replica->rwLock.readLock();
        auto res = replica->ds->carryOutOperation(op, arg1, arg2);
        replica->rwLock.readUnlock();
        return res;
}

template <typename K, typename V>
void BlackBox<K, V>::updateFromLog(const int tid, long long endIndex)
{
        auto replica = &replicas[tid / MAX_THREADS_PER_NODE];
        if(endIndex <= replica->tail) return;

        volatile long long important = replica->tail;
        for (int i = replica->tail; i < endIndex; i++)
        {
                int idx = i % LOG_SIZE;

                if (UNLIKELY(idx == 0 && i != 0))
                {
                        replica->empty = !replica->empty;
                }
                while (log[idx].emptyBit == (i / LOG_SIZE % 2 == 0 ? false : true))
                {
                        //Wait for the slot to be filled
                }

                replica->ds->carryOutOperation(log[idx].op, log[idx].arg1, log[idx].arg2);
        }
        __sync_synchronize();

     // printf("%d updates from %lld to %lld\n", tid, replica->tail, endIndex);
        replica->tail = endIndex; //No CAS required, you need the write lock to be in this function...
}

template <typename K, typename V>
bool BlackBox<K, V>::validate()
{
        cout << "Overall Summary" << endl;
        cout << "\tGlobal Tail: " << logTail << endl;
        cout << "\tLogMin: " << logMin << endl;

        cout << "\tCompleted Tail: " << completedTail << endl;

        for (int i = 0; i < numNodes; i++)
        {
                auto replica = &replicas[i];
                //           executeConcurrent(THREADS_PER_NODE * i, Operation::contains, (void *)1); //gets local structure up to date...
                cout << "Sumary of Node # " << i << ": " << endl;
                cout << "\tValidation " << replica->ds->validateStructure() << endl;
                cout << "\tKeysum: " << replica->ds->getSumOfKeys() << endl;
                cout << "\tLocal Tail: " << replica->tail << endl;
        }
}