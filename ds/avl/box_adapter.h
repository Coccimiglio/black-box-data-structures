/**
 * @TODO: ADD COMMENT HEADER
 */

#ifndef BST_ADAPTER_H
#define BST_ADAPTER_H

enum Operation
{
        none = -1,
        contains = 0,
        find = 1,
        insertIfAbsent = 10,
        insertReplace = 11,
        erase = 20,
        rangeQuery = 30
};

#include <iostream>
#include <csignal>
#include "avl.h"

#define DATA_STRUCTURE_T AVL<K, V>

template <typename K, typename V>
class box_adapter
{
private:
        const V NO_VALUE;
        DATA_STRUCTURE_T *const ds;
        K maxKey;
public:
        box_adapter(const K &KEY_MIN,
                   const K &KEY_MAX,
                   const V &VALUE_RESERVED)
            : NO_VALUE(VALUE_RESERVED), maxKey(KEY_MAX), ds(new DATA_STRUCTURE_T(KEY_MIN, KEY_MAX))
        {
        }

        ~box_adapter()
        {
                delete ds;
        }

        V getNoValue()
        {
                return 0;
        }

        void initThread(const int tid)
        {
                ds->initThread(tid);
        }

        void deinitThread(const int tid)
        {
                ds->deinitThread(tid);
        }

        void *carryOutOperation(Operation op, void *arg1, void *arg2)
        {
                switch (op)
                {
                case Operation::contains:
                        return (void *)contains((K)arg1);
                case Operation::find:
                        return (void *)find((K)arg1);
                case Operation::erase:
                        return (void *)erase((K)arg1);
                case Operation::insertIfAbsent:
                        return (void *)insertIfAbsent((K)arg1, (V)arg2);
                case Operation::insertReplace:
                        return (void *)insert((K)arg1, (V)arg2);
                default:
                        assert(false);
                }
        }


        bool isReadOnly(Operation op)
        {
                return (op == Operation::contains || op == Operation::find);
        }

        V insert(const K &key,  V val)
        {
                cout << "insert-replace functionality not implemented for this data structure" << endl;
                assert(false);
        }

        V insertIfAbsent(K key,  V val)
        {
               assert(key > 0 && key <= maxKey);
                auto res = ds->insertIfAbsent(key, val);
                return res;
        }

        V erase(const K &key)
        {
                assert(key > 0 && key <= maxKey);
                auto res = ds->erase(key);
                return res;
        }

        V find(const K &key)
        {
                cout << "insert-replace functionality not implemented for this data structure" << endl;
                assert(false);
        }

        bool contains(const K &key)
        {
                assert(key > 0 && key <= maxKey);
                return ds->contains(key);
        }
        int rangeQuery(const K &lo, const K &hi, K *const resultKeys, V *const resultValues)
        {
                cout << "insert-replace functionality not implemented for this data structure" << endl;
                assert(false);
        }
        void printSummary()
        {
                ds->printDebuggingDetails();
        }

        bool validateStructure()
        {
                return ds->validate();
        }

        long long getSumOfKeys() {
                return ds->getSumOfKeys();
        }

        void printObjectSizes()
        {
                std::cout << "sizes: node="
                          << (sizeof(Node<K, V>))
                          << std::endl;
        }


#ifdef USE_TREE_STATS
        class NodeHandler
        {
        public:
                typedef Node<K, V> *NodePtrType;
                K minKey;
                K maxKey;

                NodeHandler(const K &_minKey, const K &_maxKey)
                {
                        minKey = _minKey;
                        maxKey = _maxKey;
                }

                class ChildIterator
                {
                private:
                        bool leftDone;
                        bool rightDone;
                        NodePtrType node; // node being iterated over
                public:
                        ChildIterator(NodePtrType _node)
                        {
                                node = _node;
                                leftDone = node->left == NULL;
                                rightDone = node->right == NULL;
                        }
                        bool hasNext()
                        {
                                return !(leftDone && rightDone);
                        }
                        NodePtrType next()
                        {
                                if (!leftDone)
                                {
                                        leftDone = true;
                                        return node->left;
                                }
                                if (!rightDone)
                                {
                                        rightDone = true;
                                        return node->right;
                                }
                                setbench_error("ERROR: it is suspected that you are calling ChildIterator::next() without first verifying that it hasNext()");
                        }
                };

                bool isLeaf(NodePtrType node)
                {
                        return node->left == NULL && node->right == NULL;
                }
                size_t getNumChildren(NodePtrType node)
                {
                        if (isLeaf(node))
                                return 0;
                        return node->left != NULL + node->right != NULL;
                }
                size_t getNumKeys(NodePtrType node)
                {
                        if (node->key == minKey || node->key == maxKey)
                                return 0;
                        return 1;
                }
                size_t getSumOfKeys(NodePtrType node)
                {
                        if (getNumKeys(node) == 0)
                                return 0;
                        return (size_t)node->key;
                }
                ChildIterator getChildIterator(NodePtrType node)
                {
                        return ChildIterator(node);
                }
        };
        TreeStats<NodeHandler> *createTreeStats(const K &_minKey, const K &_maxKey)
        {
                return new TreeStats<NodeHandler>(new NodeHandler(_minKey, _maxKey), ds->getRoot(), true);
        }
#endif
};

#endif
