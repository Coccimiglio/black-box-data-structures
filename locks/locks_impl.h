/* 
 * File:   locks_impl.h
 * Author: trbot
 *
 * Created on June 26, 2017, 7:51 PM
 */

#ifndef LOCKS_IMPL_H
#define LOCKS_IMPL_H

static void acquireLock(volatile int *lock)
{
        while (1)
        {
                if (*lock)
                {
                        __asm__ __volatile__("pause;");
                        continue;
                }
                if (__sync_bool_compare_and_swap(lock, false, true))
                {
                        return;
                }
        }
}

static void releaseLock(volatile int *lock)
{
        *lock = false;
}

static bool readLock(volatile int *lock)
{
        return *lock;
}

struct TryLock
{
        int volatile state;
        TryLock()
        {
                state = 0;
        }
        bool tryAcquire()
        {
                if (state)
                        return false;
                return __sync_bool_compare_and_swap(&state, 0, 1);
        }
        void release()
        {
                __sync_synchronize();
                state = 0;
                __sync_synchronize();
        }
        bool isHeld()
        {
                auto ret = state;
                return ret;
        }
};

#endif /* LOCKS_IMPL_H */
